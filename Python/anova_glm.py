from statsmodels.compat.python import lrange, lmap
import numpy as np
import scipy as sp
import pandas as pd
from statsmodels.formula.formulatools import (_remove_intercept_patsy,
                                    _has_intercept, _intercept_idx)

def anova_glm(*args, **kwargs):
    """
    ANOVA table for one or more generalized linear models by Chi-Squared.
    
    Based off:
        https://github.com/statsmodels/statsmodels/blob/f3077577a983fc16217c77d98618510e7eebd66c/statsmodels/stats/anova.py
        https://svn.r-project.org/R/trunk/src/library/stats/R/anova.R
    """
    typ = kwargs.get('typ', 1)

    ### Farm Out Single model ANOVA Type I, II, III, and IV ###

    if len(args) == 1:
        model = args[0]
        return anova_single(model, **kwargs)

    try:
        assert typ in [1,"I"]
    except:
        raise ValueError("Multiple models only supported for type I. "
                         "Got type %s" % str(typ))

    ### COMPUTE ANOVA TYPE I ###

    # if given a single model
    if len(args) == 1:
        return anova_single(*args, **kwargs)

    # received multiple fitted models

    test = kwargs.get("test", "Chisq")
    scale = kwargs.get("scale", None)
    n_models = len(args)

    model_formula = []
    pr_test = "Pr(>%s)" % test
    names = ['df_resid', 'deviance', 'df_diff', 'deviance_diff', pr_test]
    table = pd.DataFrame(np.zeros((n_models, 5)), columns = names)

    if not scale: # assume biggest model is last
        scale = args[-1].scale

# From R implementation    
#    "Rao" = ,"LRT" = ,"Chisq" = {
#               dfs <- table[, "Df"]
#               vals <- table[, dev.col]/scale * sign(dfs)
#	            vals[dfs %in% 0] <- NA
#               vals[!is.na(vals) & vals < 0] <- NA # rather than p = 0
#	            cbind(table,
#                     "Pr(>Chi)" = pchisq(vals, abs(dfs), lower.tail = FALSE)
#                    )
#	   },
    
    table["deviance"] = lmap(getattr, args, ["deviance"]*n_models)
    table["df_resid"] = lmap(getattr, args, ["df_resid"]*n_models)
    table.ix[1:, "df_diff"] = -np.diff(table["df_resid"].values)
    table["deviance_diff"] = -table["deviance"].diff()
    
    if test == "Chisq":
        table[pr_test] = 1.0 - sp.stats.chi2.cdf(table["deviance_diff"]/scale, np.abs(table["df_diff"]).max())
        
    return table