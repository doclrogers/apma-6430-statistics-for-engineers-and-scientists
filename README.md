### Python Written by [David Rogers](http://www.doclrogers.com)

Suggest downloading [Anaconda scientific Python distribution](https://store.continuum.io/cshop/anaconda/) to get started.

### Based on R code from Dr. Don Brown.

Suggest downloading [R Studio](https://www.rstudio.com/) to get started.