


#******************************************************
#
#					
#			ANCOVA
#	 
#
#******************************************************


#***********************************************************
#
#			Data Loading and Preparation
#
#***********************************************************

# Source the input code

source("/Users/deb/Dropbox/department/Classes/SYS4021/sys 421 2012/Rcode/AccidentInput.R")

# Get the data as before

acts <- file.inputl("/Users/deb/Dropbox/department/Classes/SYS4021/sys 421 2012/data/TrainAccidents/")
	

# Put all years in one data frame


	comvar <- intersect(colnames(acts[[1]]), colnames(acts[[8]]))

	# Creating one data frame for all 11 years. (called totacts)
		
	totacts <- rbind(acts[[1]][, comvar])

	for(i in 2:11)
	{
		totacts <- rbind(totacts, acts[[i]][,comvar])
	}
	
	summary(totacts)

# Get the extreme points in ACCDMG


dmgbox <- boxplot(totacts$ACCDMG)


# selecting the extreme points in ACCDMG

xacts <- totacts[totacts$ACCDMG > dmgbox$stats[5],]

# Clean the data by removing duplicates.


duplicated(xacts[, c("YEAR", "MONTH", "DAY", "TIMEHR", "TIMEMIN")])

xacts.clean <- xacts[!duplicated(xacts[, c("YEAR", "MONTH", "DAY", "TIMEHR", "TIMEMIN")]),]



#***********************************************************
#
#			Qualitative Variable Modeling
#
#***********************************************************


# Cause as a predictor variable

Cause <- rep(NA, nrow(acts[[11]]))

# putting totacts in main memory

attach(xacts.clean)

Cause[which(substr(CAUSE, 1, 1) == "M")] <- "M"
Cause[which(substr(CAUSE, 1, 1) == "T")] <- "T"
Cause[which(substr(CAUSE, 1, 1) == "S")] <- "S"
Cause[which(substr(CAUSE, 1, 1) == "H")] <- "H"
Cause[which(substr(CAUSE, 1, 1) == "E")] <- "E"

# Removing the refrence to 2011
detach(xacts.clean)

# making Cause a factor

Cause <- as.factor(Cause)

train.lmq1 <- lm(ACCDMG~Cause, data = xacts.clean)

summary(train.lmq1)

# finding the base case

contrasts(Cause)


# Changing the base case to M

contrasts(Cause) <- matrix(c(1,0,0,0, 0,1,0,0, 0,0,0,0, 0,0,1,0, 0,0,0,1), byrow =T, nrow = 5)

colnames(contrasts(Cause)) <- c("E", "H", "S", "T")



# Making train type, Typeq, a predictor with freight trains as the choice and other as the base case. 

Typeq <-(xacts.clean$TYPEQ == 1)

Typeq <- as.factor(Type)

# A model with Cause and Typeq

train.lmq3 <- lm(ACCDMG~ Cause+Typeq, data = xacts.clean)

summary(train.lmq3)


# An interaction model

train.lmq4 <- lm(ACCDMG~(Cause+Typeq)^2, data = xacts.clean)

summary(train.lmq4)

# Partial F test

anova(train.lmq3, train.lmq4)

# ANCOVA  model with TRNSPD

train.lmq5 <- lm(ACCDMG~Cause+ TRNSPD, data = totacts)

summary(train.lmq5)

